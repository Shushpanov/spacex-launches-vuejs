# spacex-launches-vue

Use https://api.spacexdata.com/v2/launches API to get Space X launches
with Vue and Vuetify framework

Visit page https://shushpanov.gitlab.io/spacex-launches-vuejs/ to see project online

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
